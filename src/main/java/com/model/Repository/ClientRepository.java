/**
 * 
 */
package com.model.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.model.Model.Client;


public interface ClientRepository extends JpaRepository<Client, Long> {

}
