package com.model.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.model.Model.Compte;

public interface CompteRepository extends JpaRepository<Compte, String> {

}
